---
title: how does McMule fit in?
id: fit
next: physics
time: 120
---

* McMule is an *integrator*
* take the $\ell$-loop $n$-parton matrix element and integrate over
  PS
  * for $\ell=1$ &rarr; OpenLoops or by hand
  * for $\ell=2$ &rarr; massification of QCD results
* take care of MC (`vegas`), subtraction, measurement functions, etc
* many processes implemented at NNLO ($\mu\to\nu\bar\nu e$, partial
  $e\mu\to e\mu$, $\ell p\to\ell p$, $ee\to ee$,...)
* theory:
  * massification
    [[Engel, Gnendiger, Signer, YU 18]](https://arxiv.org/abs/1811.06461)
  * FKS$^\ell$
    [YU 19, [Engel, Signer, YU 19](https://arxiv.org/abs/1909.10244)]
  * NTS stabilisation
    [[Banerjee, Engel, Schalch, Signer, YU 21](https://arxiv.org/abs/2106.07469),
    [Engel, Signer, YU 21](https://arxiv.org/abs/2112.07570)]
* inherited code from `MENLO_PARC` [[Signer 97](https://inspirehep.net/literature/444895)]
* ... which inherited code from earlier things [Kunszt *time
  inmemoriam*]

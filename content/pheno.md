---
title: random bit of pheno
id: pheno
next: pheno2
---


<div class="grid two-columns">
  <div>

pheno example $e\mu\to e\mu$ @ NNLO
[[Banerjee, Engel, Signer, YU 20](https://arxiv.org/abs/2007.01654)]
* $E_{\rm beam} = 150{\rm GeV}$
* $E_e > 1{\rm GeV}$
* $\theta_\mu > 0.3{\rm mrad}$
* at LO: $
    \tan{\theta_\mu^{\rm el}} = \frac{2\tan{\theta_e}}{(1+\gamma^2 \tan^2{\theta_e})(1+g_\mu^\ast)-2}
  $
* where
    $g_\mu^\ast = \frac{E m +M^2}{E m + m^2}$ and $\gamma = \frac{E+m}{\sqrt{s}}$
* $0.9 < \theta_\mu / \theta_\mu^{\rm el} < 1.1$
* no chance to do by hand..

  </div>

```fortran
FUNCTION QUANT(q1,q2,q3,q4,q5,q6,q7)
q3lab = boost_rf(q1,q3)
q4lab = boost_rf(q1,q4)

theta_e = acos(cos_th(ez,q3lab))
theta_m = acos(cos_th(ez,q4lab))
Ee = q3lab(4)

!elasticity curve
theta_m_el = ...
bdev = theta_m/theta_m_el

!energy&angular cut
if(Ee<1000.) pass_cut = .false.
if(theta_m<0.3E-3) pass_cut = .false.
!band cut
if(bdev<0.9) pass_cut = .false.
if(bdev>1.1) pass_cut = .false.

names(1) = "thetae"
quant(1) = theta_e
END FUCNTION QUANT
```

  </div>
</div>

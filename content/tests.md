---
title: tests
id: tests
next: helpers
---
<style type="text/css">
.mycode {
  height: 100%;
  width: 100%;
  display: flex;
}
.term-fg-green {
  color: #090;
}
.job-log {
  width: 100%;
  height: 100%;
  font-family: "Menlo", "DejaVu Sans Mono", "Liberation Mono", "Consolas", "Ubuntu Mono", "Courier New", "andale mono", "lucida console", monospace;
  padding: 8px 12px;
  margin: 0 0 8px;
  font-size: 15px;
  word-break: break-all;
  word-wrap: break-word;
  color: #fff;
  border-radius: 2px;
  min-height: 42px;
  background-color: #111;
}
.line-number {
  color: #666;
  padding: 0 8px;
  padding-right: 8px;
  min-width: 50px;
  padding-right: 1em;
  -webkit-user-select: none;
  user-select: none;
}
.term-fg-l-cyan {
  color: #00bdbd;
}
.term-bold {
    font-weight: 600;
}
.term-fg-l-green {
  color: #00d600;
}
</style>
<div class="grid two-columns">

* McMule uses CI to build and test the code
* this is `docker`ised to ensure reproducibility
* we test against Mathematica
   * loop functions (logarithms, dilogs, etc.) in various kinematic
     regimes
   * matrix elements for fixed kinematics
* against reference values fixed at some point
   * whole integrands for a single point $x\in[0,1]^{n_{\rm dim}}$
   * short-ish integration $\mathcal{O}(10^3)$ points
   * external libraries
* split into
   * fast test while working $\mathcal{O}(1{\rm s})$
   * slow tests on `push` $\mathcal{O}(5{\rm min})$


<div class="mycode">
<code class="job-log d-block"><div><div><a class="line-number">1838</a><span class="gl-white-space-pre-wrap term-fg-green">[PASS]</span><span class="gl-white-space-pre-wrap"> eb2ebg pole at   0.00000E+00</span></div><div><a class="line-number">1839</a><span class="gl-white-space-pre-wrap term-fg-green">[PASS]</span><span class="gl-white-space-pre-wrap"> eb2ebg finite at   0.00000E+00</span></div><div><a class="line-number">1840</a><span class="gl-white-space-pre-wrap term-fg-green">[PASS]</span><span class="gl-white-space-pre-wrap"> em2emggEE tree at   0.00000E+00</span></div><div><a class="line-number">1841</a><span class="gl-white-space-pre-wrap term-fg-green">[PASS]</span><span class="gl-white-space-pre-wrap"> em2emggEE pole at   0.00000E+00</span></div><div><a class="line-number">1842</a><span class="gl-white-space-pre-wrap term-fg-green">[PASS]</span><span class="gl-white-space-pre-wrap"> em2emggEE finite at   2.22045E-16</span></div><div><a class="line-number">1843</a><span class="gl-white-space-pre-wrap term-fg-green">[PASS]</span><span class="gl-white-space-pre-wrap"> ee2eegg tree at   0.00000E+00</span></div><div><a class="line-number">1844</a><span class="gl-white-space-pre-wrap term-fg-green">[PASS]</span><span class="gl-white-space-pre-wrap"> ee2eegg pole at   0.00000E+00</span></div><div><a class="line-number">1845</a><span class="gl-white-space-pre-wrap term-fg-green">[PASS]</span><span class="gl-white-space-pre-wrap"> ee2eegg finite at   0.00000E+00</span></div><div><a class="line-number">1846</a><span class="gl-white-space-pre-wrap">Test block OpenLoops Reference complete.</span></div><div><a class="line-number">1847</a><span class="gl-white-space-pre-wrap">Passed 69/69 tests in   0.2s</span></div><div><a class="line-number">1848</a><span class="gl-white-space-pre-wrap">Test suite completed in 258.9s.</span></div><div><a class="line-number">1849</a><span class="gl-white-space-pre-wrap">passing 942/942 tests, failing 0</span></div></div><div><div role="button" class="log-line collapsible-line d-flex justify-content-between ws-normal"> <a class="line-number">1851</a> <span class="line-text w-100 gl-white-space-pre-wrap term-fg-l-cyan term-bold">Cleaning up file based variables</span> </div> </div><div><a class="line-number">1853</a><span class="gl-white-space-pre-wrap term-fg-l-green term-bold">Job succeeded</span></div></code>
</div>
</div>

---
title: quick physics interlude
id: physics
next: structure1
time: 150
---
* need to briefly talk physics
* singularities when $E_\gamma\sim\xi\to0$
* subtract universal limit
* $$\mathrm{d}\sigma_{n+1} \sim \int\mathrm{d}\xi\Big(
       \xi \mathcal{M}_ {n+1}^{(\ell)}
       - \underbrace{
           \mathcal{M}_ {n+1}^{(\ell,s)}
       }_{\mathcal{E} \mathcal{M}_n^{(\ell)}(\xi=0)}\theta(\xi-\xi_c)
   \Big)$$
* $ \mathcal{E} \sim \sum_{i,j}
    \frac{p_i\cdot p_k}{(p_i\cdot k)(p_j\cdot k)}
    {\rm sign}_{i,j} $
* this structure happens **everywhere** [Yennie, Frautschi,
  Suura 61]
* the usual sign convention: ${\rm sign}_ {i,j} = (-1)^{n_{ij}+1}$
  where $n_{ij}$ is the number of incoming particles or outgoing
  antiparticles among the particles $i$ and $j$

* group processes that share some matrix elements into process groups:
  * `mudec`, `mue`, `mudecrare`, `ee`, `gg`, `nu`

---
title: file format
id: format
next: tests
---
* store results in [custom binary format](https://mule-tools.gitlab.io/manual/#x1-1400214)
   * current iteration
   * value, error and $\chi^2$ of the integration
   * `vegas` grid
   * random seed
   * histograms
   * run time
* this allows us to resume integration & analyse data
* Fortran has record based binary format: `(uint32_t length)-(data)-(uint32_t length)`
* magic header (`\t McMule  \t`), file version (`v3N`), SHA1 of source
  code (`83df9`)
```
00000000: 0900 0000 204d 634d 756c 6520 2009 0000  .... McMule  ...
00000010: 000a 0000 0076 334e 2020 2020 2020 200a  .....v3N       .
00000020: 0000 0005 0000 0038 3364 6639 0500 0000  .......83df9....
```

---
title: function pointers
id: func
next: ps
time: 60
---
* how do I call `matel0`, I don't know how many arguments it will
  have?
* fun fact: if you call a function with too many arguments, it just
  works
* ... borderline criminal solution
* needs glue code in C to get the compiler to play along
```fortran
  FUNCTION SIGMA_0(x, wgt, ndim)
  call gen_mom(ps, x(userdim+1:ndim), masses(1:nparticle), vecs, weight)

  if(weight > zero) then
    if(cuts) then
      mat = matel0(vecs(:,1), vecs(:,2), vecs(:,3), vecs(:,4), &
                vecs(:,5), vecs(:,6), vecs(:,7))
      sigma_0 = mat * weight * userweight * symmfac * fluxfac
      call bin_it(wg, var)
    endif
  endif
  END FUNCTION SIGMA_0
```

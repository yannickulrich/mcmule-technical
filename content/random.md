---
title: random number generation
id: random
next: format
---
<div class="grid two-columns">

* used `vegas` default [Park-Miller](https://doi.org/10.1145/63039.63042) `minstd`
* $z_{k+1} = a\cdot z_k\ {\rm mod}\ m$ with $m$ prime and $2< a< m-1$.
* fixed period $p< m$, s.t. $z_{k+p}=z_k$
* $p=m-1$ if $a$ is a primitive root modulo $m$: $
    \forall g\ \text {co-prime to $m$}\quad
    \exists k\in\mathbb {Z} \quad
    \text {s.t.}
    \quad a^k\equiv g\ (\text {mod}\ m)$
* $m=2^{31}-1$, $a=7^5 = 16\,807$ is a good choice
* period is smaller than I'd like but probably still okay
* this can go horribly wrong

<div class="image" style="background-image: url(randu.gif);" >

IBM's `randu` $m=2^{31}$, $a=2^{16}+3$

</div>

</div>

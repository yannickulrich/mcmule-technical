---
title: pymule helpers
id: helpers
next: build
---
<style type="text/css">
.mycode {
}
</style>
<div class="grid two-columns">
<div>

* `pip install pymule`
* re-invented templating (`pymule preproc`, <br> `quad` and `double`)
* naive parallelisation of very similar jobs
  * creation of `menu` files (`pymule create -i`)
  * using `slurm`
  * or stupidly complicated shell script
  * ... with custom job control
  * ... that runs in `bash v4.1.(2)` from 2009
* analysis tools for loading and merging of data
* extract and collect particle ID comments <br> (`pymule ffilter`)

</div>
<div class="mycode">

menu file
```sh
conf em2em_muone_paper/em2em-muone.conf

## LO

run 88874 1.000000 em2em0 muone 0
run 47404 1.000000 em2em0 muone 0
```

config file
```sh
# specify the program
binary=mcmule

# specify the output folder
folder=em2em_muone_paper/

# Specify statistics
declare -A STAT=(
  ["em2em0"]="5000\n20\n10000\n70"
)
```

</div>
</div>

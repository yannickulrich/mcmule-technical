---
title: phase space
id: ps
next: random
---
* take $x_i \in [0,1]^{n_{\rm dim}}$ from `vegas` and get phys. momenta
* have routines up to $1\to7$ and $2\to4$ with various tunings
* $\xi$ needs to be $x_1$ to implement $\delta(\xi)$ of
  FKS$^\ell$
* numerical problems from
  $\mathcal{M}_ {n+1}^{(\ell)}
    \sim \dfrac1{q\cdot k}
    = \dfrac1{\xi^2}\dfrac1{1-\beta\cos\theta}$
* align `vegas` grid: $\cos\theta = 2x_2-1$
* problem: can't always do that &rarr; FKS-ish partitioning, e.g.
  $1=\theta(s_{15} > s_{35}) + \theta(s_{15} < s_{35})$
* delicate balance: too much tuning hurts just a much!
* after same amount of run-time about 10* times smaller error

---
title: matrix elements
id: matel
next: sign
time: 180
---
```fortran
  FUNCTION EM2EMGf_EEEE(p1,p2,q1,q2,q3)
    !! e-(p1) mu-(p2) -> e-(q1) mu-(q2) y(q3)
    !! for massive electrons
  real (kind=prec) :: p1(4),p2(4),q1(4),q2(4),q3(4)
  real (kind=prec) :: em2emgf_eeee

  ct(0) = Ieik(xicut, Epart, (/part(p1, 1, 1), part(p3, 1, -1)/), ct(-1))

  em2emgf_eeee = tree(0) * ct(0) + tree(1) * ct(-1) &
                + oneloop(0)
  if( ct(-1) * tree(0) + oneloop(-1) > 1e-3) then
    ! poles don't cancel well
  endif
  END FUNCTION EM2EMGf_EEEE
```
* takes 4-momenta as `(px, py, pz, E)` because `MENLO_PARC`
* comment indicates particle ID and is mandatory for compilation
* `real(kind=prec)` allows us to change to quad precision (except for
  3rd party libraries)

---
title: analysis & Open Science
id: analysis
next: pheno
---
<div class="grid two-columns">
  <div>

* public code under GPLv3 (private development version)
* detailed manual on [website](https://mule-tools.gitlab.io/manual/)
* published results are run in Docker container with recorded SHA1
* input files & configuration (incl. random seeds) are
  [public](https://gitlab.com/mule-tools/user-library/)
* tarball of generated data is public
* analysis pipeline is public as Jupyter notebooks
    * generated from `.py` files in git using
      [`py2nb`](https://gitlab.com/mule-tools/py2nb)
    * build into website by `nbcovert`
    * incl. [Binder](https://mybinder.org/) (cf. [my previous
      talk](https://gitlab.com/yannickulrich/binder-demo))
  </div>
<iframe class="image" src="https://mule-tools.gitlab.io/user-library//mu-e-scattering/muone-legacy/legacy.html#w/-cut" frameBorder="0"></iframe>
</div>

---
title: McMule
id: mcmule
start: true
next: fit
time: 100
---

What is McMule (**M**onte **C**arlo for **Mu**ons and other
**Le**ptons)?

 * framework for higher-order QED calculations
   using FKS$^\ell$
 * calculates *arbitrary* IR safe observables
 * written in Fortran 95 with helpers (eg. `pymule` in python)
 * [mule-tools.gitlab.io](https://mule-tools.gitlab.io/)
 *  Design goals
     * easy to use
     * easy to extend
     * (somewhat) modular
     * Open Science!
     * function pointers are awesome!

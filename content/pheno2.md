---
title: random bit of pheno
id: pheno2
---


<div class="grid two-columns">
  <div>

* analysis and data on [the website](https://mule-tools.gitlab.io/user-library/mu-e-scattering/muone-legacy/legacy.html#w/-cut)
* roughly 370 days of CPU

```python
from pymule import *
setup(folder="em2em_muone_paper/out.tar.bz2")
nlocut = scaleset(mergefks(
  sigma("em2emFEE"),
  sigma("em2emFEM"), sigma("em2emFMM"),
  sigma("em2emREE15"), sigma("em2emREE35"),
  sigma("em2emREM"), sigma("em2emRMM"),
  anyxi=sigma("em2emA")
), alpha**3*conv)

nnlocut = scaleset(mergefks(
  sigma("em2emFFEEEE"),
  sigma("em2emRFEEEE15"), sigma("em2emRFEEEE35"),
  sigma("em2emRREEEE1516"), sigma("em2emRREEEE3536"),
  sigma("em2emAFEE"), sigma("em2emAREE15"),
  sigma("em2emAREE35"),
  anyxi1=sigma("em2emAA"), anyxi2=sigma("em2emNFEE")
), alpha**4*conv)
```

  </div>

  <div class="image" style="background-image: url(muone-theta-e-yes-cut.svg);" >


  </div>
</div>


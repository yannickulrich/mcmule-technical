---
title: the usual sign
id: sign
next: func
time: 120
---
* the usual sign convention: ${\rm sign}_ {i,j} = (-1)^{n_{ij}+1}$
  where $n_{ij}$ is the number of incoming particles or outgoing
  antiparticles among the particles $i$ and $j$
* define a custom type `particle` that keeps momentum, charge, and direction
* instead of much duplicated code, developers define a *particle
   string function* and McMule does the rest
```fortran
  FUNCTION EM2EM_EE_part(n, p1, p2, p3, p4)
  n = 2
  em2em_ee_part(1:n) = (/part(p1, 1, 1), part(p3, 1, -1)/)
  END FUNCTION EM2EM_EE_part

  ...
    case("em2emRFEEEE")
      matel0 => em2emgf_eeee
      matel1 => em2emf_ee
      partfunc => em2em_ee_part
      ps => psx3_fks ; fxn => sigma_1
      nparticle = 5 ; ndim = 5
      masses(1:5) = (/ Me, Mm, Me, Mm, 0._prec /)
  ...
```

from matplotlib.animation import FuncAnimation
get_ipython().magic('pylab tk')

def lcg(m, a, z):
    while True:
        z = (a * z) % m
        yield z / float(m)

g = lcg(2**31, 2**16 + 3, 1)
dat = np.array([
    [next(g), next(g), next(g)]
    for i in range(10002)
])

ax = axes(projection='3d')
ax.scatter(dat[:,0], dat[:,1], dat[:,2],s=1)

ax.xaxis.set_ticklabels([])
ax.yaxis.set_ticklabels([])
ax.zaxis.set_ticklabels([])

def update(i, fig, ax):
    ax.view_init(elev=-160, azim=i)
    return fig, ax

anim = FuncAnimation(
    gcf(),
    update,
    frames=np.concatenate((
        np.linspace(-100, -130, 70),
        np.linspace(-130, -100, 70)
    )),
    repeat=True, fargs=(gcf(), ax)
)

anim.save('randu.gif', dpi=320, writer='imagemagick', fps=24)
